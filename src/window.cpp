#include "window.hpp"

Window::Window() {
    this->window = new sf::RenderWindow(sf::VideoMode(1280, 720), "Pong");
    
    sf::Vector2u winSize = this->window->getSize();

    this->score.setPosition({(float)(winSize.x / 2), 10.f});
    this->setup(); 
}

Window::~Window() {
    delete this->window;
}

void Window::run() {
    while(this->window->isOpen()) {
        this->update();
        this->render();
    }
}

void Window::setup() {
    this->ball.stop();
    this->score.setVisible(true);

    sf::Vector2u winSize = this->window->getSize();

    this->player1.setPosition({50.f, (float)(winSize.y / 2)});
    this->player2.setPosition({winSize.x - 50.f, (float)(winSize.y / 2)});
    this->ball.setPosition({(float)(winSize.x / 2), (float)(winSize.y / 2)});
}

void Window::update() {
    sf::Event event;

    while(this->window->pollEvent(event)) {
        if(event.type == sf::Event::Closed) {
            this->window->close();
        }
    }

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
        this->player2.move(-1.f);

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
        this->player2.move(1.f);

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::W))
        this->player1.move(-1.f);

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::S))
        this->player1.move(1.f);
    
    sf::Vector2f ballPosition = this->ball.getPosition();
    sf::Vector2u windowSize = this->window->getSize();

    if(ballPosition.y >= windowSize.y || ballPosition.y <= 0) 
        this->ball.setDirections(1, -1);

    if(this->ball.getBounds().intersects(this->player1.getBounds()) || this->ball.getBounds().intersects(this->player2.getBounds())) 
        this->ball.setDirections(-1, 1);
    
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && this->ball.isStopped()) {
        this->ball.start();
        this->score.setVisible(false);
    }
    
    if(ballPosition.x >= windowSize.x) {
        this->score.update(1, 0);
        this->setup();
    }

    if(ballPosition.x <= 0) {
        this->score.update(0, 1);
        this->setup();
    }
    
    this->ball.move();
}

void Window::render() {
    this->window->clear();
    this->player1.render(*window); 
    this->player2.render(*window);
    this->ball.render(*window);
    this->score.render(*window);
    this->window->display();
}


