#include "score.hpp"

Score::Score() {
    this->p1_Score = 0;
    this->p2_Score = 0;
    this->visible = false;

    this->font.loadFromFile("../src/fonts/Arial.TTF");
    this->text.setFont(this->font);
    this->text.setString(std::to_string(this->p1_Score) + " : " + std::to_string(this->p2_Score));
    this->text.setCharacterSize(100);
}

void Score::update(const unsigned int p1, const unsigned int p2) {
    this->p1_Score += p1;
    this->p2_Score += p2;

    this->text.setString(std::to_string(this->p1_Score) + " : " + std::to_string(this->p2_Score));
};

void Score::render(sf::RenderTarget& target) {
    if(visible)
        target.draw(this->text);
}

void Score::setVisible(bool visible) {
    this->visible = visible;
};

void Score::setPosition(sf::Vector2f pos) {
    pos.x = pos.x - (this->text.getLocalBounds().width / 2);
    this->text.setPosition(pos);
}
