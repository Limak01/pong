#pragma once

#include <SFML/Graphics.hpp>

class Ball {
    sf::RectangleShape shape;
    sf::Vector2f directions;
    float size;
    float speed;
    bool stopped = false;

    public:
        Ball();

        void render(sf::RenderTarget& target);
        void setPosition(sf::Vector2f position);
        void move();
        void setDirections(const int x, const int y);
        sf::Vector2f getPosition();
        const sf::FloatRect getBounds();
        void stop();
        void start();
        bool isStopped();
};
