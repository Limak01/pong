#include "player.hpp"

Player::Player() {
    this->moveSpeed = 2.f;

    this->shape.setSize({15.f, 80.f});
    this->shape.setFillColor(sf::Color::White);
}

void Player::render(sf::RenderTarget& target) {
    target.draw(this->shape);
}

void Player::setPosition(sf::Vector2f position) {
    sf::Vector2f shapeSize = this->shape.getSize();

    position.y = position.y - (shapeSize.y / 2);

    this->shape.setPosition(position);
}

void Player::move(float upOrDown) {
    this->shape.move(0.f, this->moveSpeed * upOrDown);
}

const sf::FloatRect Player::getBounds() {
    return this->shape.getGlobalBounds();
}
