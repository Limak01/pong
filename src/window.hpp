#pragma once

#include <SFML/Graphics.hpp>
#include "player.hpp"
#include "ball.hpp"
#include "score.hpp"
#include <iostream>

class Window {
    sf::RenderWindow* window;
    Player player1;
    Player player2;
    Ball ball;
    Score score;

    public:
        Window();
        ~Window();

        void run();
        void update();
        void render();
        void setup();
};
