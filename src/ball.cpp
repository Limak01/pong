#include "ball.hpp"

Ball::Ball() {
    this->size = 12.f;
    this->speed = 2.f;
    this->directions.x = -1;
    this->directions.y = -1;
    this->shape.setSize({this->size, this->size});
    this->shape.setFillColor(sf::Color::White);
}

void Ball::render(sf::RenderTarget& target) {
    target.draw(this->shape);
}

void Ball::setPosition(sf::Vector2f position) {
    position.x = position.x - (this->size / 2);
    position.y = position.y - (this->size / 2);
    this->shape.setPosition(position);
} 

void Ball::move() {
    if(!this->stopped)
        this->shape.move(speed * directions.x, speed * directions.y);
}

void Ball::setDirections(const int x, const int y) {
    this->directions.x = this->directions.x * x;
    this->directions.y = this->directions.y * y;
}

sf::Vector2f Ball::getPosition() {
    return this->shape.getPosition();
}

const sf::FloatRect Ball::getBounds() {
    return this->shape.getGlobalBounds();
}

void Ball::stop() {
    this->stopped = true;
}

void Ball::start() {
    this->stopped = false;
}

bool Ball::isStopped() {
    return this->stopped;
}
