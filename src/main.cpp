#include <SFML/Graphics.hpp>
#include "window.hpp"

int main() {
    Window game;
    game.run();

    return 0;
}
