#pragma once

#include <SFML/Graphics.hpp>

class Score {
    unsigned int p1_Score;
    unsigned int p2_Score;
    bool visible;
    sf::Font font;
    sf::Text text;

    public:
        Score();
        void update(const unsigned int p1, const unsigned int p2);
        void render(sf::RenderTarget& target);
        void setVisible(bool visible);
        void setPosition(sf::Vector2f pos);
};
