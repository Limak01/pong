#pragma once

#include <SFML/Graphics.hpp>

class Player {
    sf::RectangleShape shape;
    float moveSpeed;

    public:
        Player();
        
        void render(sf::RenderTarget& target);
        void setPosition(sf::Vector2f position);
        void move(float upOrDown);
        const sf::FloatRect getBounds();
};
